package currency;

public class Yen implements Currency {
    @Override
    public String getSymbol() {
        return "JPY";
    }

    @Override
    public String getName() {
        return "Japanese Yen";
    }
}
