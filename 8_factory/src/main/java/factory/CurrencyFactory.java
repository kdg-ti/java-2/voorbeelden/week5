package factory;

import currency.Currency;
import currency.Euro;
import currency.USDollar;
import currency.Yen;

public class CurrencyFactory {
    private CurrencyFactory() {
        // Blocked
    }

    public static Currency createCurrency(String country) {
        if (country.equalsIgnoreCase("België")) {
            return new Euro();
        } else if (country.equalsIgnoreCase("Japan")) {
            return new Yen();
        } else if (country.equalsIgnoreCase("US")) {
            return new USDollar();
        } else if (country.equalsIgnoreCase("France")) {
            return new Euro();
        }
        //...
        throw new IllegalArgumentException("No such currency");
    }
}
