package currency;

public class USDollar implements Currency {
    @Override
    public String getSymbol() {
        return "USD";
    }

    @Override
    public String getName() {
        return "US Dollar";
    }
}
