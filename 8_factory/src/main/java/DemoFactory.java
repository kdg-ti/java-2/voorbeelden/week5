import currency.Currency;
import factory.CurrencyFactory;

public class DemoFactory {
    public static void main(String[] args) {
        Currency currency = CurrencyFactory.createCurrency("België");
        System.out.printf("%-10s --> %-3s\n", currency.getName(), currency.getSymbol());
    }
}
