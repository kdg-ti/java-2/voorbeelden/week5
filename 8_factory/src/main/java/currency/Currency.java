package currency;

public interface Currency {
    String getSymbol();
    String getName();
}
