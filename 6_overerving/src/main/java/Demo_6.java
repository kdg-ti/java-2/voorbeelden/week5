import observer.ObserveerbaarPunt;
import observer.PuntObserver;

public class Demo_6 {
    public static void main(String[] args) {
        ObserveerbaarPunt punt = new ObserveerbaarPunt(1, 2);
        PuntObserver observer = new PuntObserver(punt);
        punt.addObserver(observer);

        punt.verdubbelX();
        punt.verdubbelY();
    }
}
