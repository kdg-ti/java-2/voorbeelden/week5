package currency;

/**
 * Mark Goovaerts
 * 14/10/2019.
 */
public class Euro implements Currency {
    @Override
    public String getSymbol() {
        return "EUR";
    }

    @Override
    public String getName() {
        return "Euro";
    }
}
