package currency;

public class SwissFranc implements Currency {
    @Override
    public String getSymbol() {
        return "CHF";
    }

    @Override
    public String getName() {
        return "Swiss Franc";
    }
}
