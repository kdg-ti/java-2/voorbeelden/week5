# Voorbeelden java 2 - Week 5
* 1 Singleteon (lazy initialization)
* 2 Singleton (klassiek)
* 3 Singleton (Enum)
* 4 Observer
* 5 Delegatie
* 6 Overerving
* 7 Static Factory
* 8 Static Factory
